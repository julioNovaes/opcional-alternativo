package br.com.projeto;

public interface Mapeador<T,S> {

    S aplicar( final T t );

}
