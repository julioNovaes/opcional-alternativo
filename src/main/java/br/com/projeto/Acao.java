package br.com.projeto;

@FunctionalInterface
public interface Acao {

    void executar();

}
