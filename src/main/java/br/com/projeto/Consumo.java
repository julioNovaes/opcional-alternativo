package br.com.projeto;

@FunctionalInterface
public interface Consumo<T> {

    void consumir( final T t );

}