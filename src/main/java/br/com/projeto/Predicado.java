package br.com.projeto;

public interface Predicado<T> {

    boolean teste( final T t );

}

