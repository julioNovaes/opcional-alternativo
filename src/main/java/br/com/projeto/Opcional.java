package br.com.projeto;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Stream;



public final class Opcional<T> {

    private static final Opcional<Object> OPCIONAL_NULO = new Opcional<>(null);

    private final T t;

    private Opcional(final T t) {
        this.t = t;
    }


    public static <E> Opcional<E> de(final E e) {

        return new Opcional<>(Objects.requireNonNull(e));

    }

    public static <E> Opcional<E> possivelNulo(final E e){
        return new Opcional<>(e);
    }

    public void sePresente(final Consumo<? super T> consumo) {

        if (presente()) consumo.consumir(t);

    }

    public Opcional<T> seNaoPresente(final Acao acao) {

        if (vazio()) acao.executar();

        return this;

    }

    @SuppressWarnings("unchecked")
    public Opcional<T> filtrar(final Predicado<? super T> predicado) {

        if (presente() && predicado.teste(t)) {

            return new Opcional<>(t);

        }

        return (Opcional<T>) OPCIONAL_NULO;

    }

    public T obter() {

        if (vazio()) throw new NoSuchElementException();

        return t;

    }

    @SuppressWarnings("unchecked")
    public <S> Opcional<S> mapear(final Mapeador<? super T,S> mapeador){

        Objects.requireNonNull(mapeador);

        if(presente())

            return new Opcional<>(mapeador.aplicar(t));

        return (Opcional<S>) OPCIONAL_NULO;
    }

    public void sePresenteOuNao(Consumo<? super T> consumo,Acao acao){

        if(presente()){

            consumo.consumir(t);

        }else {

            acao.executar();

        }
    }

    public T ou(final T outro){

        if(vazio()) return outro;

        return t;
    }
    public T ouObtem(final Provedor<? extends T> provedor){

        if(vazio()) return provedor.prover();

        return t;
    }

    public boolean presente(){
        return t != null;
    }

    public boolean vazio(){
        return !presente();
    }

    public<X extends Throwable> T ouExcecao(final Provedor<? extends X > provedor) throws X {

        if(vazio()) throw provedor.prover();

        return t;
    }

    @Override
    public int hashCode() {
        return Objects.requireNonNull(t).hashCode();
    }

    @Override
    public String toString() {
        return Objects.requireNonNull(t).toString();
    }

    @Override
    public boolean equals(final Object obj) {
        return Objects.requireNonNull(t).equals(obj);
    }

    public Stream<T> stream(){
        return Stream.ofNullable(t);
    }

}