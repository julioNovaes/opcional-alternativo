package br.com.projeto;

@FunctionalInterface
public interface Provedor<T> {

    T prover();

}
