package br.com.projeto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class OpcionalTest {

    @Test
    void de() {

        Assertions
                .assertEquals(Integer.class, Opcional.de(10).obter().getClass());

    }

    @Test
    void deQuandoNulo() {

        Assertions
                .assertThrows(NullPointerException.class, () -> Opcional.de(null));

    }

    @Test
    void possivelNulo() {

        Assertions
                .assertEquals(Integer.class, Opcional
                        .de(10)
                        .obter()
                        .getClass());

    }

    @Test
    void possivelNuloQuandoNulo() {

        final AtomicBoolean status = new AtomicBoolean(false);

        Opcional
                .possivelNulo(null)
                .seNaoPresente(() -> status.set(true));

        Assertions.assertTrue(status.get());

    }

    @Test
    void possivelNuloQuandoNuloExecesso() {

        final AtomicBoolean status = new AtomicBoolean(false);

        final Opcional<Object> objectOpcional = Opcional
                .possivelNulo(null)
                .seNaoPresente(() -> status.set(true));

        Assertions
                .assertThrows(NoSuchElementException.class, objectOpcional::obter);
    }

    @Test
    void sePresente() {

        final AtomicBoolean status = new AtomicBoolean(true);
        final AtomicInteger valo = new AtomicInteger(0);

        Opcional.de(10)
                .seNaoPresente(() -> status.set(false))
                .sePresente(integer -> valo.incrementAndGet());

        Assertions.assertTrue(status.get());
        Assertions.assertEquals(1, valo.get());
    }

    @Test
    void seNaoPresente() {

        final AtomicBoolean status = new AtomicBoolean(false);
        final AtomicInteger valo = new AtomicInteger(0);

        Opcional
                .de(100)
                .filtrar(integer -> integer < 10)
                .seNaoPresente(() -> status.set(true))
                .sePresente(integer -> valo.set(1000));

        Assertions.assertTrue(status.get());
        Assertions.assertEquals(0, valo.get());
    }

    @Test
    void filtrar() {

        final AtomicBoolean status = new AtomicBoolean(false);
        final AtomicInteger valo = new AtomicInteger(0);
        Opcional
                .de(100)
                .filtrar(integer -> integer > 10)
                .seNaoPresente(() -> status.set(true))
                .sePresente(integer -> valo.set(1000));

        Assertions.assertFalse(status.get());
        Assertions.assertEquals(1000, valo.get());
    }

    @Test
    void mapear() {

        Assertions.assertTrue(Opcional.de(100).mapear(integer -> integer != 0).obter());
        Assertions.assertFalse(Opcional.de(50).mapear(integer -> integer > 600).obter());

    }

    @Test
    void mapearNulo() {

        final AtomicBoolean status = new AtomicBoolean(false);

        Opcional.possivelNulo(null).mapear(o -> null).seNaoPresente(() -> status.set(true));

        Assertions.assertTrue(status.get());

    }


    @Test
    void sePresenteOuNao() {

        final AtomicBoolean status = new AtomicBoolean(false);
        final AtomicInteger valo = new AtomicInteger(0);

        Opcional.de(100).sePresenteOuNao(s -> status.set(true), System.out::println);

        Opcional.possivelNulo(null).sePresenteOuNao(o -> valo.set(100), () -> valo.set(10));

        Assertions.assertTrue(status.get());
        Assertions.assertEquals(10, valo.get());

    }

    @Test
    void ou() {

        final AtomicInteger valor = new AtomicInteger(100);

        Assertions.assertEquals(100, Opcional
                .de(valor)
                .ou(new AtomicInteger(0)).get());

    }

    @Test
    void ouSegundaOpcao() {

        final AtomicInteger valor = new AtomicInteger(100);

        Assertions.assertEquals(0, Opcional
                .de(valor)
                .filtrar(atomicInteger -> atomicInteger.get() < 10)
                .ou(new AtomicInteger(0)).get());
    }

    @Test
    void ouObtem() {

        final AtomicBoolean status = new AtomicBoolean(false);

        Assertions.assertTrue(Opcional
                .possivelNulo(status)
                .filtrar(AtomicBoolean::get)
                .ouObtem(() -> new AtomicBoolean(true))
                .get());
    }

    @Test
    void ouObtemComValor() {
        final AtomicBoolean status = new AtomicBoolean(false);

        Assertions.assertFalse(Opcional
                .possivelNulo(status)
                .ouObtem(() -> new AtomicBoolean(true))
                .get());
    }

    @Test
    void ouExcecao() {

        Assertions.assertTrue(Opcional
                .possivelNulo(true)
                .ouExcecao(NullPointerException::new));

    }

    @Test
    void ouExcecaoComExececao() {

        Assertions.assertThrows(RuntimeException.class, () -> Opcional.possivelNulo(null).ouExcecao(RuntimeException::new));
    }

    @Test
    void OpcionalhashCode(){

        Assertions.assertEquals(Integer.valueOf(10).hashCode(),Opcional.de(10).hashCode());
    }

    @Test
    void OpcionalToString(){

        Assertions.assertEquals(Double.toString(10.00),Opcional.de(10.00).toString());
    }

    @Test
    void OpcionalEquals(){

        Assertions.assertTrue(Opcional.de("").equals(""));
    }

}

